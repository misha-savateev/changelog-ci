# Changelog generator
## Point:
The goal is to provide users and contributors with a clear view of the significant modifications 
that have occurred in each edition or version of the project, in order to facilitate their understanding.
## Usage:
1. Open your project's .gitlab-ci.yml file.
2. Add the following code to the script section of your .gitlab-ci.yml file

``script: - curl https://gitlab.com/misha-savateev/changelog-ci/-/blob/main/changelog-gen.sh | sh``

3. Save the changes to your .gitlab-ci.yml file.
4. Whenever you push changes to your repository, GitLab will automatically execute the changelog-gen.sh script and generate a changelog file named CHANGELOG.md.

