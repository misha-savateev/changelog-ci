#!/bin/sh

MAJOR=0
MINOR=0
PATCH=0

CURRENT_VERSION="${MAJOR}.${MINOR}.${PATCH}"

while read COMMIT
do
  if echo ${COMMIT} | grep -iq "BREAKING CHANGE:"; then
    MAJOR=$((${MAJOR} + 1))
    MINOR=0
    PATCH=0
  fi
  if echo ${COMMIT} | grep -iq "feat:"; then
    MINOR=$((${MINOR} + 1))
    PATCH=0
  fi
  if echo ${COMMIT} | grep -iq "fix:"; then
    PATCH=$((${PATCH} + 1))
  fi
  CURRENT_VERSION="${MAJOR}.${MINOR}.${PATCH}"

  if [ "${PREVIOUS_VERSION}" != "${CURRENT_VERSION}" ];
  then
    CHANGELOG="${TEXT}\n${CHANGELOG}"
    TEXT="### Version [${CURRENT_VERSION}]\n- ${COMMIT}\n"
  else
    TEXT="${TEXT}- ${COMMIT}\n"
  fi

  PREVIOUS_VERSION=${CURRENT_VERSION}
done < <(git log --reverse --oneline --pretty=format:'%h - %s'; echo)

CHANGELOG="# Changelog\n\n${TEXT}\n${CHANGELOG}"

echo -e "${CHANGELOG}" | head -n -3 > CHANGELOG.md
echo ${CURRENT_VERSION}
